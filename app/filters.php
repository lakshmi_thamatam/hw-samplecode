<?php

	/*
	|--------------------------------------------------------------------------
	| Application & Route Filters
	|--------------------------------------------------------------------------
	|
	| Below we will find the "before" and "after" events for the application
	| which may be used to do any work before or after a request into the
	| application. Here we may also register our custom route filters.
	| @author : Venkata Lakshmi
	*/

	App::before(function($request)
	{
		// $apdObject is a Singleton object
		App::singleton('apdObject', function(){
			$app = new stdClass;
			$app->title = "APD | Dealership Invoicing";
			if (Auth::check()) {
				$app->user = Auth::User();
				$app->isLogedin = TRUE;
			}
			else
			{
				$app->isLogedin = FALSE;
				$app->user = FALSE;
			}
			return $app;
		});
		$app = App::make('apdObject');
		View::share('apdObject', $app);
	});


	App::after(function($request, $response)
	{
		/* The below headers are used to restrict the browser to cache the pages.*/
		$response->headers->set("Cache-Control","no-cache,no-store, must-revalidate");
		$response->headers->set("Pragma", "no-cache"); //HTTP 1.0
		$response->headers->set("Expires"," Sat, 26 Jul 1986 05:00:00 GMT");
	});

	/*
	|--------------------------------------------------------------------------
	| Authentication Filters
	|--------------------------------------------------------------------------
	|
	| The following filters are used to verify that the user of the current
	| session is logged into this application. The "basic" filter easily
	| integrates HTTP Basic authentication for quick, simple checking.
	|
	*/

	Route::filter('auth', function()
	{
		if (Auth::guest())
		{
			if (Request::ajax())
			{
				return Response::make('common.unauthorized');
			}
			else
			{
				return Redirect::guest('login');
			}
		}
	});
	
	
	/*
	|--------------------------------------------------------------------------
	| Login Filters
	|--------------------------------------------------------------------------
	|
	| This filter is used to check whether the current session user logged in or not.
	| Based on the loggedin user's role, application will show the relevant pages.
	*/
	
	Route::filter('login', function()
	{
		if(Auth::check())
		{
			$roleId = Auth::User()->role_id;
			$redirectPage = '';
			switch ($roleId)
			{
				case 1:
					$redirectPage = '/manage_users';
					break;
				case 2:
					$redirectPage = '/products';
					break;
				case default:
					$redirectPage = '/work_log';
					break;
			}
			return Redirect::to($redirectPage);
		}
	});

	Route::filter('checkRole', function($route, $request, $roles){
		$roleArray = explode('-', $roles);
		$userData = Auth::user();    
		if(!in_array($userData->role_id, $roleArray))
		{
			return View::make('common.unauthorized');
		}    
	});


	Route::filter('auth.basic', function()
	{
		return Auth::basic();
	});

	/*
	|--------------------------------------------------------------------------
	| CSRF Protection Filter
	|--------------------------------------------------------------------------
	|
	| The CSRF filter is responsible for protecting our application against
	| cross-site request forgery attacks. If this special token in a user
	| session does not match the one given in this request, we'll bail.
	|
	*/

	Route::filter('csrf', function()
	{
		if (Session::token() !== Input::get('_token'))
		{
			throw new Illuminate\Session\TokenMismatchException;
		}
	});