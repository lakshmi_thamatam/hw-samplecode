<?php

	/*
	|--------------------------------------------------------------------------
	| Common Functions
	|--------------------------------------------------------------------------
	|
	|   @author : Vankata Lakshmi
	*/

	/**
	 * This function will encode the html data.
	 * @param string $string
	 * @return string
	 */
	function htmlEncode($string)
	{
		return htmlentities(trim($string), ENT_QUOTES, "UTF-8");
	}

	/**
	 * This function will decode the html data.
	 * @param $string string
	 * @return string
	 */
	function htmlDecode($string)
	{
		return html_entity_decode($string, ENT_QUOTES);    
	}

	/**
	 * This function is used to identify the Character set and convert the string to UTF-8 supported string.
	 * @param $base_string string
	 * @return string
	 */
	function convertToUTF($base_string = '')
	 {
		if ($base_string != '') 
		{
			$base_string = mb_convert_encoding($base_string, "UTF-8", mb_detect_encoding($base_string, "UTF-8, ASCII, ISO-8859-1, ISO-8859-2, ISO-8859-3, ISO-8859-4, ISO-8859-5, ISO-8859-6, ISO-8859-7, ISO-8859-8, ISO-8859-9, ISO-8859-10, ISO-8859-13, ISO-8859-14, ISO-8859-15, ISO-8859-16, Windows-1251, Windows-1252, Windows-1254", true));
		}

		return $base_string;
	}

	/*
	 * This function is used to encode or decode the parameters passed based on the flag.
	 * @params $flag int
	 * @params $dataInput string
	 * @return string
	 */
	function encodeDecodeParams($flag = 1, $dataInput='')
	{
		$changed = $dataInput;
		if(!empty($dataInput))
		{
			$changed = ($flag == 1)?base64_encode($dataInput):base64_decode($dataInput);
		}
		
		return $changed;
	}
