<?php

class CommonController extends Controller {
	
	/*
    |--------------------------------------------------------------------------
    | Common Controller
    |--------------------------------------------------------------------------
    |	This file acts a a intermediate layer between the controllers and the db.
    |	This controller is created in keeping the idea of using the same functions for both APIs and the HTML pages.
	|
    |   @author : Vankata Lakshmi
    */

    protected $return_array = array();
    
	/**
     * This function is used to define the layout for the HTML pages.
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }
    
    /** Cars related functions. Start **/
    
    /**
     * This function is used to list all the cars in the system.
     * @return array
     */
    protected function getAllCars()
    {
        $this->return_array = array();
        $this->return_array['status'] = 100;
        $this->return_array['message'] = 'Cannot process your request now.';
        $this->return_array['data'] = array();
        
        $carsResult = with(new Cars)->getAllCars();
        
        if(is_array($carsResult) && !empty($carsResult))
        {
            $this->return_array['status'] = 200;
            $this->return_array['message'] = 'Data available.';
            $this->return_array['data'] = $carsResult;
        }
        
        return $this->return_array;       
    }
    
    /**
     * This function is used to get all the car makes.
     * @return array
     */
    protected function getAllMakes()
    {
        $this->return_array = array();
        $this->return_array['status'] = 100;
        $this->return_array['message'] = 'Data not available at this moment.';
        $this->return_array['data'] = array();
        
        $makeData['status'] = array(1);
        $makeResult = with(new Cars)->getMakeDetails($makeData);
        if(is_array($makeResult) && !empty($makeResult))
        {
            $this->return_array['status'] = 200;
            $this->return_array['message'] = 'Data available.';
            $this->return_array['data'] = $makeResult;
        }
        
        return $this->return_array;
    }
    
    /**
     * This function is used to insert the car data into the database.
     * @param $carData array
     * @return array
     */
    protected function saveCar($carData)
    {
        $this->return_array = array();
        $this->return_array['status'] = 100;
        $this->return_array['message'] = 'Data not sufficient to proceed.';
        $this->return_array['data'] = array();
        
        if(is_array($carData) && !empty($carData))
        {
            $carsInsertData = $this->generateCarsData($carData);
            if(is_array($carsInsertData) && !empty($carsInsertData))
            {
				$carsData['tableName'] = 'apd_cars';
				$carsData['data'] = $carsInsertData;
                $saveResult = with(new Commons)->insertRecords($carsData);
                if($saveResult)
                {
                    $this->return_array['status'] = 200;
                    $this->return_array['message'] = 'Car data inserted successfully.';
                }
                else
                {
                    $this->return_array['message'] = 'Unable to insert car data. Please try after sometime.';
                }
            }
            else
            {
                $this->return_array['message'] = 'Car data already existed.';
            }
        }
        return $this->return_array;
    }
    
    /**
     * This function is used to update the car data
     * @param $carData array
     * @return array
     */
    protected function modifyCar($carData)
    {
        $this->return_array = array();
        $this->return_array['status'] = 100;
        $this->return_array['message'] = 'Data not sufficient to proceed.';
        $this->return_array['data'] = array();
        
        if(is_array($carData) && !empty($carData))
        {
            $makeName = (isset($carData['make_name']) && !empty($carData['make_name']))?htmlEncode(ucfirst(trim($carData['make_name']))):htmlEncode(ucfirst(trim($carData['make_hidden'])));
            $modelName = (isset($carData['model_name']) && !empty($carData['model_name']))?htmlEncode(strtoupper(trim($carData['model_name']))):htmlEncode(strtoupper(trim($carData['model_hidden'])));
            
            $updateData['whereClause'] = array('make'=>htmlEncode(ucfirst(trim($carData['make_hidden']))),'model'=>htmlEncode(strtoupper(trim($carData['model_hidden']))));
            $updateData['upadateData'] = array('make'=>$makeName,'model'=>$modelName);
            
			$updateData['tableName'] = 'apd_cars';
            $updateFlag = with(new Commons)->updateRecords($updateData);
            
            if($updateFlag)
            {
                $this->return_array['status'] = 200;
                $this->return_array['message'] = 'Car data updated successfully.';
            }
            else
            {
                $this->return_array['message'] = 'There is no change in the data to update.';
            }
        }
        return $this->return_array;
    }

    /**
     * This function is used to generate the car data according to the database format.
     * @param $carsData array
     * @return array
     */
    private function generateCarsData($carsData)
    {
        $carRequriedData['make'] = htmlEncode(trim($carsData['make_name']));
        $carRequriedData['model'] = htmlEncode(trim($carsData['model_name']));
        $carYears = with(new Cars)->getMakeModelData($carRequriedData);
        $carYearsArray = array();
        if(is_array($carYears) && !empty($carYears))
        {
            $carYearsArray = explode(",", $carYears[0]->years);
        }
        $carsDataArray = array();
        for($i=$carsData['start_year'];$i<=$carsData['end_year'];$i++)
        {
            $carsChunkArray = array();
            if(is_array($carYearsArray) && (empty($carYearsArray) || !in_array($i, $carYearsArray)))
            {
                $carsChunkArray['make'] = htmlEncode(ucfirst(trim($carsData['make_name'])));
                $carsChunkArray['model'] = htmlEncode(strtoupper(trim($carsData['model_name'])));
                $carsChunkArray['year'] = $i;
                $carsChunkArray['status'] = 1;
            }
            
            if(is_array($carsChunkArray) && !empty($carsChunkArray))
            {
                array_push($carsDataArray, $carsChunkArray);
            }
        }
        return $carsDataArray;
    }
    
    /**
     * This funtion is used to get the year of a make and model.
     * @param $carData array
     * @return array
     */
    protected function getCarYears($carData)
    {
        $this->return_array = array();
        $this->return_array['status'] = 100;
        $this->return_array['message'] = 'Data not sufficient to proceed.';
        $this->return_array['data'] = array();
        
        if(is_array($carData) && !empty($carData))
        {
            $yearsResult = with(new Cars)->getMakeYears($carData);
            if(is_array($yearsResult) && !empty($yearsResult))
            {
                $this->return_array['status'] = 200;
                $this->return_array['message'] = 'Data available.';
                $this->return_array['data'] = $yearsResult;
            }
        }        
        return $this->return_array;
    }
    
    /**
     * This function is used to update the cars data.
     * @param array $requiredData
     * @return array
     */
    protected function changeCarYear($requiredData)
    {
        $this->return_array = array();
        $this->return_array['status'] = 100;
        $this->return_array['message'] = 'Could not perform the action. Please try after some time';
        $this->return_array['data'] = array();
        
        if(is_array($requiredData) && !empty($requiredData))
        {
            if($requiredData['type'] == 3 && $requiredData['data']['logged'] != 1)
            {
                $deleteData['where_column'] = 'id';
                $deleteData['where_data'] = encodeDecodeParams(2,$requiredData['data']['car']);
                
                $deleteStatus = with(new Cars)->removeCarYear($deleteData);
                if($deleteStatus)
                {
                    $this->return_array['status'] = 200;
                    $this->return_array['message'] = 'Car data deleted successfully.';
                }
            }
            else if($requiredData['type'] == 1 || $requiredData['type'] == 2)
            {
                $updateData['whereClause'] = array('id'=>encodeDecodeParams(2,$requiredData['data']['car']));
                $status = ($requiredData['type'] == 1)?1:0;
                $updateData['upadateData'] = array('status'=>$status);
                $updateData['tableName'] = 'apd_cars';
				
                $updateFlag = with(new Commons)->updateRecords($updateData);
                
                if($updateFlag)
                {
                    $this->return_array['status'] = 200;
                    $this->return_array['message'] = 'Car data updated successfully.';
                }
            }
        }
        
        return $this->return_array;
    }
	
	/**
     * This function is used to list all the cars in the system.
     * @return array
     */
    protected function allCarsList()
    {
        $this->return_array = array();
        $this->return_array['status'] = 100;
        $this->return_array['message'] = 'Cannot process your request now.';
        $this->return_array['data'] = array();
        
        $carsResult = with(new Cars)->getAllCars();
        
        if(is_array($carsResult) && !empty($carsResult))
        {
            $this->return_array['status'] = 200;
            $this->return_array['message'] = 'Data available.';
            $this->return_array['data'] = $carsResult;
        }
        
        return $this->return_array;       
    }
    
    /** Cars related functions. End **/
}
