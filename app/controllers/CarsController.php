<?php

class CarsController extends CommonController {

    /*
    |--------------------------------------------------------------------------
    | Cars Controller
    |--------------------------------------------------------------------------   
    |
    |   @author : Vankata Lakshmi
    */
    
    /**
     * This function is used to show the Cars Home Page.
     * @return	view
     */
    public function carsHome()
    {
        return View::make('cars.cars_home',array('pageTitle' => 'Manage Cars'));
    }
    
    /**
     * This function will return the cars Grid.
	 * @return json response
     */
    public function carsGrid()
    {
        $cars_data = $this->allCarsList();
        $carDetails = array();
        if(isset($cars_data['status']) && $cars_data['status'] == 200)
        {
            $carDetails = $cars_data['data'];
        }
        $viewPage = View::make('cars.cars_grid')->with('grid_data',$carDetails)->render();
        return Response::json(array('output'=>$viewPage));
    }
    
    /**
     * This function will open the add / edit cars popup.
     * @return json response
     */
    public function carsPopup()
    {
        $params = Input::all();
        $popupData['mode'] = $params['mode'];
        $popupData['form_id'] = 0;
        $popupView = 'add_cars';
        $make_details = $this->getAllMakes();
        $makesList = $this->createMakesList($make_details);
        if($params['mode'] == 2)
        {
            $popupView = 'edit_car';
            $popupData['form_id'] = preg_replace('/[^A-Za-z0-9\-]/', '_', $params['model']);
            $popupData['makes'] = $makesList;
            $popupData['make'] = encodeDecodeParams(2,$params['make']);
            $popupData['model'] = encodeDecodeParams(2,$params['model']);
        }
        else if($params['mode'] == 1)
        {
            $yearsList = $this->getYears();
            $popupData['makes'] = $makesList;
            $popupData['years'] = $yearsList;
        }
        else
        {
            $popupView = 'add_year';
            $popupData['form_id'] = 'year';
            $years = $this->getYears(1);
            $popupData['years'] = $years;
        }
        $popup = View::make('cars.'.$popupView)->with('cars_data',$popupData)->render();
        return Response::json(array('dialog' => $popup));
    }
    
    /**
     * This function will create the makes list.
     * @param $makesData array
     * @return string
     */
    public function createMakesList($makesData)
    {
        $makeList = '<option value="">Select make</option>';
        if(isset($makesData['status']) && $makesData['status'] == 200)
        {
            foreach($makesData['data'] as $makes)
            {
                $makeList .= '<option value="'.htmlEncode($makes->make).'">'.$makes->make.'</option>';
            }
        }
        
        $makeList .= '<option value="add">Other</option>';
        return $makeList;
    }
    
    /**
     * This function will return the years option.
     * @return string
     */
    public function getYears($addFlag = 0)
    {
        $yearCount = ($addFlag)?5:3;
        $yearsLimit = ($addFlag)?120:100;
        $maxYear = date('Y', strtotime('+'.$yearCount.' years'));
        $currentYear = date('Y');
        $yearString = '<option value="">Select year</option>';        
        for($i=$yearsLimit;$i>0;$i--)
        {
            $selected = '';
            if($maxYear == $currentYear)
            {
                $selected = 'selected="selected';
            }
            $yearString .= '<option '.$selected.' value="'.$maxYear.'">'.$maxYear.'</option>';
            $maxYear--;
        }
        
        return $yearString;
    }
    
    /**
     * This function is used to change the status of the car.
     * @return json reponse
     */
    public function changeStatus()
    {
        $postStatus = Input::all();
        $saveResult = $this->changeCarYear($postStatus);
        
        return Response::json($saveResult);
    }

    /**
     * This function is used to save the car data into the data base.
     * @return json response
     */
    public function addCar()
    {
        $inputCarData = Input::all();
        $saveResult = $this->saveCar($inputCarData);
        return Response::json($saveResult);
    }
    
    /**
     * This function is used to save the car data into the data base.
     * @return json response
     */
    public function updateCar()
    {
        $inputCarData = Input::all();
        $saveResult = $this->modifyCar($inputCarData);
        return Response::json($saveResult);
    }
    
    /**
     * This function is used to create the cars select data based on the selected year
     * @return json response.
     */
    public function getCarsByYears()
    {
        $postData = Input::all();
        $carContent = $this->getYearCars($postData);        
        return Response::json(array('output'=>$carContent));
    }
	
	/**
     * This function is used to give the provision for the user to download the CSV file.
     * @return json response.
     */   
    public function downloadSampleCsv()
    { 
        $filePath = Config::get('constants.sample_csv_files_path');
        $xlsFile = $filePath.'download_sample.csv';
        return Response::json(array("downloadFlag"=>1,"path"=>$xlsFile));
    }
    
}
