<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where we can register all of the routes for an application.
|
*/

Route::get('/', function(){
	return Redirect::to('login');
});

Route::resource('user', 'UserController');

Route::post('/user/store','UserController@store');

Route::get('logout', 'UserController@destroy');

Route::group(array('before' => 'auth'), function()
{   
    Route::get('/profile','UserController@showProfile');
    Route::resource('/profile/upload/{mode}/{type}/','UserController@uploadImage');
    Route::get('/profile/cropImage/','UserController@cropImage');
    Route::post('/profile/unlink/','UserController@deleteTempImage');
    Route::post('/profile/saveCroppedImage/','UserController@saveUserImage');
    Route::post('/profile/update/','UserController@updateProfile');
    
   
    /* Only Super Admin and Office Staff can Access the following routes. */
    Route::group(array('before' => 'checkRole:1-2'), function()
    { 
        /* Manage Cars section. Start */
        
		Route::get("/manage_cars","CarsController@carsHome");
        Route::post("/manage_cars/get_cars","CarsController@carsGrid");
        Route::post("/manage_cars/popup","CarsController@carsPopup");
        Route::post("/manage_cars/add_car","CarsController@addCar");
        Route::post("/manage_cars/update_car","CarsController@updateCar");
        Route::post("/manage_cars/change_status","CarsController@changeStatus");
        Route::post("/manage_cars/get_year_cars","CarsController@getCarsByYears");
		Route::post("/cars/download_sample_csv","CarsController@downloadSampleCsv");
		
        /* Manage Cars section. End */
        
    });
   
});