<?php


	/*
    |--------------------------------------------------------------------------
    | Common Data Base Functions file
    |--------------------------------------------------------------------------
    |	This file will consists of all the common DB functions.
    |	Create, Update, Delete will be called from this file.
    |	
    |   @author : Vankata Lakshmi
    */

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Commons extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;
    
    
    /**
     * This function is used to store the records in to the supplied table
     * @param array $insertData
     * @return boolean
     */
    public function insertRecords($insertData = array())
    {
        $result = 0;
        if(is_array($insertData) && !empty($insertData))
        {
            $result = DB::table($insertData['tableName'])->insert($insertData['data']);
        }
        return $result;
    }    
    
    /**
     * This function is used to update the records of the supplied table
     * @param $updateData array
     * @return boolean
     */
    public function updateRecords($updateData = array())
    {
        $updateFlag = 0;
        if(is_array($updateData) && !empty($updateData))
        {
            $updateObj = DB::table($updateData['tableName']);
            foreach($updateData['whereClause'] as $key => $value)
            {
                $updateObj->where($key,$value);
            }
            $updateFlag = $updateObj->update($updateData['upadateData']);
        }
        
        return $updateFlag;
    }
    
}
