<?php


	/*
    |--------------------------------------------------------------------------
    | Cars DataBase Functions file
    |--------------------------------------------------------------------------
    |	This file will consists of all the cars related DB functions.
    |	
    |   @author : Vankata Lakshmi
    */

	use Illuminate\Auth\UserTrait;
	use Illuminate\Auth\UserInterface;
	use Illuminate\Auth\Reminders\RemindableTrait;
	use Illuminate\Auth\Reminders\RemindableInterface;

	class Cars extends Eloquent implements UserInterface, RemindableInterface {

		use UserTrait, RemindableTrait;

		/**
		 * The database table used by the model.
		 *
		 * $cars string
		 */    
		protected $cars = 'apd_cars';    
		
		/**
		 * This function will return all cars with the make years grouped.
		 * @return result set (array)
		 */
		public function getAllCars()
		{
			$carResult = array();
			DB::statement("SET group_concat_max_len = 10240");
			$carsObj = DB::table($this->cars.' as c');
			$carsObj->select("c.make","c.model",DB::raw("group_concat(' ',c.year ORDER BY c.year ASC) as year"));
			$carsObj->groupby("c.make");
			$carsObj->groupby("c.model");
			
			$carResult = $carsObj->get();
			
			return $carResult;
		}
		
		/**
		 * This function will return the list of all makes in an ascending order.
		 * @param $requredData array
		 * @return result set (array)
		 */
		public function getMakeDetails($requredData)
		{
			$resultSet = array();
			if(is_array($requredData) && !empty($requredData))
			{
				$carsObj = DB::table($this->cars.' as c');
				$carsObj->select('c.make');
				$carsObj->whereIn('c.status',$requredData['status']);
				$carsObj->groupby('c.make');
				$carsObj->orderby('c.make','asc');
				
				$resultSet = $carsObj->get();
			}
			
			return $resultSet;
		}
		
		/**
		 * This function is used to return the make years as a string result set, if the intended car (supplied as parameter in the array) exists.
		 * @param $requredData array
		 * @return result set (array)
		 */
		public function getMakeModelData($requredData)
		{
			$resultSet = array();
			if(is_array($requredData) && !empty($requredData))
			{
				$carObj = DB::table($this->cars.' as c');
				$carObj->select(DB::raw('group_concat(year) as years'));
				$carObj->where('c.make','=',$requredData['make']);
				$carObj->where('c.model','=',$requredData['model']);
				
				$resultSet = $carObj->get();
			}
			
			return $resultSet;
		}
		
		/**
		 * This function is used to remove the car data.
		 * @param $carData array
		 * @return boolean
		 */
		public function removeCarYear($carData)
		{
			if(is_array($carData) && !empty($carData))
			{
				return DB::table($this->cars)->where($carData['where_column'],'=',$carData['where_data'])->delete();
			}
		}
		
		/**
		 * This function is used to get all the required cars for the selected years.
		 * @param $requiredData array
		 * @return array
		 */
		public function getYearCars($requiredData)
		{
			$result = array();
			DB::statement("SET group_concat_max_len = 10240");
			if(is_array($requiredData) && !empty($requiredData))
			{
				$carsObj = DB::table($this->cars.' as c');
				$carsObj->select("c.make","c.model",DB::raw("GROUP_CONCAT(c.year ORDER BY c.year ASC) as yearsList"));           
				$carsObj->groupby("c.make");
				$carsObj->groupby("c.model");
				$carsObj->having("yearsList","not like","%".$requiredData['selectedYear']."%");
				$carsObj->orderby("c.make");
				$carsObj->orderby("c.model");
				
				$result = $carsObj->get();
			}
			return $result;
		}
		
		/**
		 * This function verifies whether car existed or not.
		 * @param $carData array
		 * @return boolean
		 */
		public function checkCarExistance($carData)
		{
			$returnFlag = 0;
			if(is_array($carData) && !empty($carData))
			{
				$carObj = DB::table($this->cars);
				$carObj->select("id");
				$carObj->where("make","=",$carData['make']);
				$carObj->where("model","=",$carData['model']);
				$carObj->where("year","=",$carData['year']);
				
				$returnFlag = $carObj->count();            
			}        
			return $returnFlag;
		}
	}
