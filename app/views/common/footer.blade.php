<!-- Bootstrap -->
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/AdminLTE/app.js" type="text/javascript"></script>
<script type="text/javascript">    
    
    var mainSegment = "<?php echo Request::segment(1); ?>";    
    $(".sidebar-menu>li.active").removeClass("active");
    $("#"+mainSegment+"_menu").addClass("active");
    
    /**
     * This function will return today date as "mm/dd/yyyy" format     
     * @returns string
     */
    function getToday()
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }
        if(mm<10) {
            mm='0'+mm
        }
        return today = mm+'/'+dd+'/'+yyyy;
    }
    
    /**
     * This function will give the last week date from today.
     * @returns date
     */
    function getLastWeekDate(range)
    {
        var period = 7;
        if(typeof range == 'undefined')
        {
            range = 1;
        }
        
        switch(range)
        {
            case 1:
                period = 7;
                break;
            case 2:
                period = 30;
                break;
        }
        
        var date = new Date();
        date.setDate(date.getDate() - period);
        
        var month = date.getMonth()+1;
        var dayDate = date.getDate();
        var modifiedDate = ((month < 10)?'0'+month:month) +'/'+((dayDate < 10)?'0'+dayDate:dayDate)+'/'+date.getFullYear();
        
        return modifiedDate;
    }
    
    /**
     * This function will send an Ajax request to the server to change the status of the item.
     * @param {int} type
     * @param {int} recordId
     * @param {string} table
     * @param {string} column
     * @param {string} callFunction
     * @returns void
     */
    function changeStatus(type,recordId,table,column,callFunction,functionParams)
    {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/change_status",
            data: {type: type, recordId: recordId, tableName: table, column: column},
            success: function(data){                           
                if(data.status === 200)
                {                                
                    makeDynamicFunctionCall(callFunction,functionParams);
                }
                else
                {
                    alert(data.message);
                }
            }
        });
    }

    /**
     * This function is used to call the Dyamic function from the changeStatus function. 
     * @param {string} functionName
     * @returns void
     */
    function makeDynamicFunctionCall(functionName,functionParams)
    {
        if(typeof functionParams != 'undefined')
        {
            this[functionName](functionParams['role'],functionParams['div_id'],functionParams['url']);
        }
        else
        {
            this[functionName]();
        }
    }
    
    /**
     * This function is used to clear the popup div when user click the close button
     * @param string divName
     * @returns void
     */
    function cleanPopupDiv(divName)
    {
        var divId = $('.modal').prop("id");
        $("#"+divName).remove('#'+divId);
        $('#'+divId).remove();
    }
    
    /**
     * This function is used to call the users grids based on the role.
     * @param {int} roleId
     * @param {string} updateDiv
     * @param {string} url
     * @return void
     */
    function callUsers(roleId, updateDiv, url)
    {        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: { role: roleId },
            beforeSend: function(){
                $('#'+updateDiv).html('<img src="<?php echo Config::get('constants.essentials_path'); ?>/img/ajax-loader.gif" class="loader_position" />');
            },		
            success: function(data){               
                $('#'+updateDiv).html('');
                $('#'+updateDiv).html(data.data);
            }
        });
    }
    
    /**
     * This function is used to fire the popup
     * @param {int} popupMode
     * @param {int} userRole
     * @param {string} url
     * @param {int} userId
     * @returns Html    
     *
     **/
    function callUserPopup(popupMode,userRole,url,userId)
    {
        var params = '';
        if(typeof(userId) !== 'undefined')
        {
            params = {mode: popupMode, role: userRole, userId: userId};
        }
        else
        {
            params = {mode: popupMode, role: userRole};
        }
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: params,
            success: function(data) {                
                if (data.dialog) {
                    var $modal = $(data.dialog);
                    $('#popup_div').append($modal);
                    $modal.filter('.modal').modal();
                } else {
                    alert(data.dialog);
                }

            }
        });
    }
    
    /**
     * This function is used to call the Dyamic function from the changeAssignment function. 
     * @param {string} name functionName
     * @param {array} name functionParams
     * @returns void
     */
    function makeDynamicTabLoad(functionName,functionParams)
    {
        if(typeof functionParams !== 'undefined')
        {
            this[functionName].apply(null,functionParams);
        }
        else
        {
            this[functionName]();
        }
    }
    
    /**
     * This function is used to load the requested Tab.
     * @param {type} userData
     * @param {type} type
     * @param {type} confirm_label
     * @param {type} dynamic_content
     * @param {type} dynamic_function
     * @returns {undefined}     */
    function loadTab(id,url,tabName,tab,div_id)
    {        
        var requiredUrl = url+id+"/"+tab+"/"+tabName;       
        $.ajax({
           datatype: "json",
           type: "GET",
           url:requiredUrl,
           beforeSend: function(){
                $('#'+div_id).html('<img src="<?php echo Config::get('constants.essentials_path'); ?>/img/ajax-loader.gif" class="loader_position" />');
            },		
            success: function(data){               
                $('#'+div_id).html('');
                $('#'+div_id).html(data.output);
            }
        });
    }
    
    /**
     * This function is used to load the requested Tab.
     * @param string id
     * @param string url
     * @param int tab
     * @param string div_id
     * @returns {undefined}     */
    function loadInvoiceTab(id,url,tab,div_id,user_id)
    {
        var userParam = (typeof user_id != 'undefined')?'/'+user_id:'';
        var requiredUrl = url+id+"/"+tab+userParam;
        $.ajax({
           datatype: "json",
           type: "GET",
           url:requiredUrl,
           beforeSend: function(){
                $('#'+div_id).html('<img src="<?php echo Config::get('constants.essentials_path'); ?>/img/ajax-loader.gif" class="loader_position" />');
            },		
            success: function(data){               
                $('#'+div_id).html('');
                $('#'+div_id).html(data.output);
            }
        });
    }
    
    function phonenumberFormatwithPos(ele,e){
         var currentPos =ele.getCursorPosition();
            ele.val(ele.val()
                .match(/\d*/g).join('')
                .match(/(\d{0,3})(\d{0,3})(\d{0,4})(\d{0,5})/).slice(1).join('-')
                .replace(/-*$/g, ''));
            if((e.which != 37 && e.which != 8) && currentPos ==ele.val().length-1){
                if( ele.val().length >3){
                    currentPos += 1;
                }
                if( ele.val().length >7){
                    currentPos += 2;
                }
                if( ele.val().length >12){
                    currentPos += 3;
                }
            }
            ele.setCursorPosition(currentPos);
    
    }
    (function ($, undefined) {
    $.fn.getCursorPosition = function() {
        var el = $(this).get(0);
        var pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
    $.fn.setCursorPosition = function(pos) {
        this.each(function(index, elem) {
          if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
          } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
          }
        });
        return this;
    };
})(jQuery); 

</script>