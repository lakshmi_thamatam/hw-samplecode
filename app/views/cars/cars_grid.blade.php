@if(is_array($grid_data) && !empty($grid_data))
<table id="cars" class="table table-bordered table-striped">    
    <thead>
        <tr>
            <th width="15%">Make</th>
            <th width="20%">Model</th>
            <th width="55%">Year</th>
            <th width="10%">Actions</th>
        </tr>
    </thead>    
    <tbody>
        @foreach($grid_data as $data)
            <tr>                
                <td><?php echo $data->make; ?></td>
                <td><?php echo $data->model; ?></td>
                <td><?php echo $data->year; ?></td>
                <td><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo base64_encode($data->model); ?>" onclick="callPopup(2,'<?php echo base64_encode($data->make); ?>','<?php echo base64_encode($data->model); ?>');"><i class="fa fa-edit" ></i> &nbsp;Edit</a></td>
            </tr>
        @endforeach
</tbody>    
</table>
@else
    <div align="center">No records found.</div>
@endif

<script type="text/javascript">
    $(document).ready(function() {        
        $('#cars').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "iDisplayLength": 100,
            "aoColumnDefs": [
                                { 'bSortable': false, 'aTargets': [ 3 ] },
                                { "bSearchable": false, 'aTargets': [ 3 ] }
                            ]
        });
    });    
    
</script>