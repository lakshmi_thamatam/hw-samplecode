@extends('layouts.master')
@section('body_content')
<section class="content">
    <div class="box">
        <div class="box-header">
                <div class="row">
                <div class="col-lg-8"><h3 class="box-title">Manage Cars</h3></div>
                <div class="col-lg-4 box-button" align="right">
                    <button class="btn btn-success" data-toggle="modal" data-target="#myModalyear" onclick="callPopup(3);"><i class="fa fa-plus" ></i> &nbsp;Add Year</button>
                    <button class="btn btn-success" data-toggle="modal" data-target="#myModal0" onclick="callPopup(1);"><i class="fa fa-plus" ></i> &nbsp;Add Car</button></div>
                <div class="clearfix"></div>
            </div>

            <div class="div_line"></div>
        </div>
        <div id="popup_div">
            
        </div>
        <div class="box-body table-responsive" id="cars_grid">
            
        </div>
    </div>
</section>
<!-- page script -->
<script type="text/javascript">
    $(document).ready(function() {        
        call_cars_grid();
    });
    
    function call_cars_grid()
    {
        $.ajax({
            url: "/manage_cars/get_cars",
            datatype: "json",
            type: "POST",
            beforeSend: function(){
                $("#cars_grid").html('<img src="<?php echo Config::get('constants.essentials_path'); ?>/img/ajax-loader.gif" class="loader_position" />');
            },
            success: function(data){                
                $("#cars_grid").html(data.output);
            }
        });
    }
	
    /**
     * This function is used to fire the add car popup
     * @param {int} popupMode
     * @param {int} productId
     * @returns Html    
     *
     **/
    function callPopup(popupMode,make,model)
    {
        var params = '';
        if(typeof(make) !== 'undefined' && typeof(model) !== 'undefined')
        {
            params = {mode: popupMode, make: make, model: model};
        }
        else
        {
            params = {mode: popupMode};
        }
        $.ajax({
            url: '/manage_cars/popup',
            dataType: 'json',
            type: 'POST',
            data: params,
            success: function(data) {                
                if (data.dialog) {
                    var $modal = $(data.dialog);
                    $('#popup_div').append($modal);
                    $modal.filter('.modal').modal();
                } else {
                    alert("Oops... Something is not right. Please try after some time.");
                }
            }
        });
    }
    
    function generateCarYears()
    {
        var make = $("#make_name").val();
        var model = $("#model_name").val();

        if(model != '' && typeof(model) != 'undefined' && make != '' && typeof(make) != 'undefined' && make != 'add')
        {
            $.ajax({
                url: '/manage_cars/get_years',
                dataType: 'json',
                type: 'POST',
                data: {make: make, model: model},
                success: function(data) {                    
                    if(data.output)
                    {
                        $("#years_div").html(data.output);
                    }
                    else
                    {
                        alert("Sorry Error.");
                    }
                } 
            });
        }
        else
        {
            alert('No enough data to generate the years.');
        }
    }
    
    function changeYear(data, type, labelName)
    {
        switch(type)
        {
            case 'activate':
                assignmentType = 'activate';
                typeVal = 1;
                break;
            case 'deactivate':
                assignmentType = 'deactivate';
                typeVal = 2;
                break;
            case 'remove':
                assignmentType = 'remove';
                typeVal = 3;
                break;
                
        }
        
        if(confirm('Are you sure, you want to '+assignmentType+' the '+labelName+'.'))
        {
            $.ajax({
                url: '/manage_cars/change_status',
                dataType: 'json',
                type: 'POST',
                data: {data: data, type: typeVal},
                success: function(data) {
                    alert(data.message);
                    generateCarYears();
                }
            });
        }
    }
    
    function get_year_cars()
    {
        var selecteYear = $("#select_year").val();
        if(selecteYear != '')
        {
            $.ajax({
                url: '/manage_cars/get_year_cars',
                dataType: 'json',
                type: 'POST',
                data: {selectedYear: selecteYear},
                beforeSend: function() {
                    $("#year_cars_div").html('<img src="<?php echo Config::get('constants.essentials_path'); ?>/img/ajax-loader.gif" class="loader_position" />');
                },
                success: function(data) {
                    if(data.output)
                    {
                        $("#year_cars_div").html('');
                        $("#year_cars_div").html(data.output);
                    }
                }
            });
        }
    }
    
</script>
@stop
